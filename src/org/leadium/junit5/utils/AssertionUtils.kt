package org.leadium.junit5.utils

import org.junit.jupiter.api.Assertions
import java.io.File


/**
 * Method asserts name of given file
 */
fun File.assertName(name: String) {
    Assertions.assertTrue(this.name.contains(name), "File name is incorrect!")
}

/**
 * Method checks the string matches of the given regular expression
 */
fun String.assertRegex(regex: Regex) {
    Assertions.assertTrue(
        this.matches(regex),
        "String: '${this}' isn't matches of the given regular expression: ${regex.pattern}"
    )
}