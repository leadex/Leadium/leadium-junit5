package org.leadium.junit5.displayname

import org.junit.jupiter.api.DisplayNameGenerator
import org.leadium.core.utils.replaceCamelCaseWithSpaces
import java.lang.reflect.Method

/**
 * {@code DisplayNameGenerator} defines the SPI for generating display
 * names programmatically.
 *
 * <p>An implementation must provide an accessible no-arg constructor.
 */
class DisplayNameGenerator : DisplayNameGenerator {

    /**
     * Generate a display name for the given {@link Nested @Nested} inner test class.
     *
     * @param nestedClass the class generate a name for; never {@code null}
     * @return the display name of the container; never {@code null} or blank
     */
    override fun generateDisplayNameForNestedClass(nestedClass: Class<*>?): String {
        return nestedClass!!.simpleName.replaceCamelCaseWithSpaces()
    }

    /**
     * Generate a display name for the given method.
     *
     * @implNote The class instance passed as {@code testClass} may differ from
     * the returned class by {@code testMethod.getDeclaringClass()}: e.g., when
     * a test method is inherited from a super class.
     *
     * @param testClass the class the test method is invoked on; never {@code null}
     * @param testMethod method to generate a display name for; never {@code null}
     * @return the display name of the test; never {@code null} or blank
     */
    override fun generateDisplayNameForMethod(testClass: Class<*>?, testMethod: Method): String {
        return testMethod.getDisplayName()
    }

    /**
     * Generate a display name for the given top-level or {@code static} nested test class.
     *
     * @param testClass the class generate a name for; never {@code null}
     * @return the display name of the container; never {@code null} or blank
     */
    override fun generateDisplayNameForClass(testClass: Class<*>?): String {
        return testClass!!.simpleName.replaceCamelCaseWithSpaces()
    }
}

/**
 * Get and format a display name for the given method.
 */
fun Method.getDisplayName(): String {
    val testCaseClass = this.getAnnotation(TestCaseDisplayName::class.java).case.java
    return testCaseClass.toDisplayName()
}

/**
 * Format a display name for the given class.
 */
fun Class<*>.toDisplayName(): String {
    val sb = StringBuilder()
    this.canonicalName
        .replaceCamelCaseWithSpaces()
        .replace("_", " ")
        .split(".")
        .reversed()
        .forEach { s: String -> sb.append(s).append(" | ") }
    return sb.toString().substring(0, sb.toString().length - 3)
}