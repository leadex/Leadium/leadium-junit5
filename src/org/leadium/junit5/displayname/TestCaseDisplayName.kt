package org.leadium.junit5.displayname

import kotlin.reflect.KClass

/**
 * The annotation uses to declare a custom display
 * name for the annotated test method
 */
@Target(AnnotationTarget.FUNCTION)
annotation class TestCaseDisplayName(
    val case: KClass<*>
)