package org.leadium.junit5

import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.extension.AfterTestExecutionCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.leadium.core.data.Status
import org.leadium.core.utils.createNewFile
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

/**
 * Junit5 callback extension for aggregating failed tests to text file.
 */
class FailedTestsAggregator(val userDir: String) : AfterTestExecutionCallback {

    /**
     * @suppress
     */
    override fun afterTestExecution(context: ExtensionContext) {
        if (executionExceptionIsPresent(context) and !isRepeated(context))
            writeTestMethodToFile(context)
    }

    private fun writeTestMethodToFile(context: ExtensionContext) {
        val file = getFile(context)
        createNewFile(file)
        FileWriter(file, true).use { fileWriter ->
            BufferedWriter(fileWriter).use { bufferedWriter ->
                bufferedWriter.write("--tests ${formatRequiredTestMethod(context)} ")
            }
        }
    }

    private fun getFile(context: ExtensionContext): File {
        return File("$userDir/${detectTestStatus(context)}.txt")
    }

    private fun detectTestStatus(context: ExtensionContext): String {
        return if (isSkipped(context)) when (isTimeout(context)) {
            true -> Status.TIMEOUT.string
            else -> Status.SKIPPED.string
        }
        else when (isAssertionError(context)) {
            true -> Status.FAILED.string
            else -> Status.BROKEN.string
        }
    }

    private fun executionExceptionIsPresent(context: ExtensionContext): Boolean {
        return context.executionException.isPresent
    }

    private fun isSkipped(context: ExtensionContext): Boolean {
        return context.executionException.get().message!!.contains("Assumption failed")
    }

    private fun isTimeout(context: ExtensionContext): Boolean {
        return context.executionException.get().message!!.contains("Timeout")
    }

    private fun isAssertionError(context: ExtensionContext): Boolean {
        return context.executionException.get() is AssertionError
    }

    private fun formatRequiredTestMethod(context: ExtensionContext): String {
        return context.requiredTestMethod.toString().split("void ")[1].split("(")[0]
    }

    private fun isRepeated(context: ExtensionContext): Boolean {
        return context.element.get().isAnnotationPresent(RepeatedTest::class.java)
    }
}