package org.leadium.junit5

import org.apache.commons.lang3.tuple.Pair
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolutionException
import org.junit.jupiter.api.extension.ParameterResolver
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.lang.reflect.Parameter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

@Target(AnnotationTarget.VALUE_PARAMETER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Inject

enum class TestDataType() {

    PROPERTY, FILE
}

data class PropertyTestData(val map: LinkedHashMap<String, Pair<String, String>>, val file: File)

class TestDataPreparation {

    private val rootProjectPath = System.getProperty("rootProjectPath")
    private val envTestDataDir = System.getProperty("envTestDataDir")

    fun getPropertyData(resourcesPath: String, propertyRelativePath: String): PropertyTestData {
        val linkedHashMap = LinkedHashMap<String, Pair<String, String>>()
        val properties = Properties()
        val propertyFile = File("$rootProjectPath/$resourcesPath/$envTestDataDir/$propertyRelativePath")
        BufferedReader(FileReader(propertyFile)).use { properties.load(it) }
        properties.forEach { k, v ->
            linkedHashMap[k as String] = Pair.of(v as String, "")
        }
        return PropertyTestData(linkedHashMap, propertyFile)
    }

    fun getFiles(resourcesPath: String, propertyRelativePath: Array<String>): Array<out Any> {
        val arrayListOfFilesTestData = ArrayList<File>()
        propertyRelativePath.forEach {
            arrayListOfFilesTestData.add( File("$rootProjectPath/$resourcesPath/$envTestDataDir/$propertyRelativePath"))
        }
        return arrayListOfFilesTestData.toArray()
    }
}

class TestDataResolver : ParameterResolver {

    @Target(AnnotationTarget.FUNCTION)
    annotation class Settings(
        val type: TestDataType,
        val resourcesPath: String,
        val relativePath: Array<String>
    )

    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Boolean {
        return parameterContext.isAnnotated(Inject::class.java)
    }

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any {
        val methodIsPresent = extensionContext.testMethod.isPresent
        var testDataType = TestDataType.PROPERTY
        if (methodIsPresent) {
            val settingsIsPresent = extensionContext.testMethod.get().isAnnotationPresent(Settings::class.java)
            if (settingsIsPresent) {
                testDataType = extensionContext.testMethod.get().getAnnotation(Settings::class.java).type
            }
        }
        return when (testDataType) {
            TestDataType.FILE -> resolveFile(parameterContext.parameter, extensionContext)
            else -> resolveProperty(parameterContext.parameter, extensionContext)
        }
    }

    private fun resolveProperty(parameter: Parameter, extensionContext: ExtensionContext): Any {
        val type: Class<*> = parameter.type
        val methodIsPresent = extensionContext.testMethod.isPresent
        if (methodIsPresent) {
            val settings = extensionContext.testMethod.get().getAnnotation(Settings::class.java)
            val resourcesPath = settings.resourcesPath
            val relativePath = settings.relativePath
            if (PropertyTestData::class.java == type) return TestDataPreparation().getPropertyData(
                resourcesPath,
                relativePath[0]
            )
            throw ParameterResolutionException("No PropertyTestData implemented for $type")
        }
        throw NoSuchElementException("No value present")
    }

    private fun resolveFile(parameter: Parameter, extensionContext: ExtensionContext): Any {
        val type: Class<*> = parameter.type
        val methodIsPresent = extensionContext.testMethod.isPresent
        if (methodIsPresent) {
            val settings = extensionContext.testMethod.get().getAnnotation(Settings::class.java)
            val resourcesPath = settings.resourcesPath
            val relativePath = settings.relativePath
            if (Array<File>::class.java == type) return TestDataPreparation().getFiles(
                resourcesPath,
                relativePath
            )
            throw ParameterResolutionException("No Array<File> implemented for $type")
        }
        throw NoSuchElementException("No value present")
    }
}